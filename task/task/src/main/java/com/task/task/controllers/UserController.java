package com.task.task.controllers;

import com.task.task.entity.User;
import com.task.task.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserController {
    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("personList")
    public String index(Model model) {
        model.addAttribute("persons", userRepository.findAll());
        return "personList";
    }

    @GetMapping("addPerson")
    public String goToAddPerson(Model model) {
        User user = new User();
        model.addAttribute("userForm", user);
        return "addPerson";
    }

    @PostMapping("addPerson")
    public String addUser(@ModelAttribute("userForm") User user) {
        userRepository.save(user);
        return "redirect:/personList";
    }

    @GetMapping("updatePerson/{id}")
    public String goToUpdateUser(@PathVariable("id") String id, Model model) {
        model.addAttribute("user", userRepository.findById(id).orElse(null));
        return "updatePerson";
    }

    @PostMapping("updateThisPerson/{id}")
    public String updateUser(@PathVariable("id") String id, User newUser) {
        User user = userRepository.findById(id).orElse(null);
        user.setName(newUser.getName());
        user.setEmail(newUser.getEmail());
        userRepository.save(user);
        return "redirect:/personList";
    }

    @PostMapping("deletePerson/{id}")
    public String deleteUser(@PathVariable("id") String id) {
        userRepository.deleteById(id);
        return "redirect:/personList";
    }
}